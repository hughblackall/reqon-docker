FROM node:20

RUN npm install --global reqon

EXPOSE 8080 8081

ENTRYPOINT reqon --save-max 1000 --save-file /data/db.json --files-dir /data/files